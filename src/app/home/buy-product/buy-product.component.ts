import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ProductService } from '../product.service';

@Component({
  selector: 'app-buy-product',
  templateUrl: './buy-product.component.html',
  styleUrls: ['./buy-product.component.scss'],
})
export class BuyProductComponent implements OnInit {
  product: any;
  constructor(private route: ActivatedRoute, private productService: ProductService) {}

  ngOnInit(): void {
    let id = this.route.snapshot.paramMap.get('id');
    this.productService.getProduct(id).subscribe((res) => {
      this.product = res;
    });
  }
}
