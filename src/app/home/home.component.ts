import { Component, OnInit } from '@angular/core';
import { uniqBy } from 'lodash';

import { ProductService } from './product.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss'],
})
export class HomeComponent implements OnInit {
  products: any[] = [];
  category: any[] = [];
  isLoading = false;
  selected = '';
  constructor(private productService: ProductService) {}

  ngOnInit() {
    this.isLoading = true;
    this.productService.getProducts('').subscribe((res: any) => {
      this.products = res;
      this.isLoading = false;
      this.category = uniqBy(res, function (e) {
        return e.category;
      });
    });
  }
  getFilterPro(category: any) {
    this.selected = category;
    this.productService.getProducts(category).subscribe((res: any) => {
      this.products = res;
      this.isLoading = false;
    });
  }
}
