import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { map, catchError } from 'rxjs/operators';
import { environment } from '@env/environment';

@Injectable({
  providedIn: 'root',
})
export class ProductService {
  constructor(private httpClient: HttpClient) {}

  getProducts(q: any) {
    return this.httpClient.get(environment.serverUrl + '/product/get-products?q=' + q);
  }
  getProduct(id: any) {
    return this.httpClient.get(environment.serverUrl + '/product/get-product/' + id);
  }
}
