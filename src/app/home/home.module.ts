import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TranslateModule } from '@ngx-translate/core';

import { SharedModule } from '@shared';
import { HomeRoutingModule } from './home-routing.module';
import { HomeComponent } from './home.component';
import { BuyProductComponent } from './buy-product/buy-product.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

@NgModule({
  imports: [CommonModule, NgbModule, TranslateModule, SharedModule, HomeRoutingModule],
  declarations: [HomeComponent, BuyProductComponent],
})
export class HomeModule {}
