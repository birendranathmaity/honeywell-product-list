import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { marker } from '@biesbjerg/ngx-translate-extract-marker';

import { HomeComponent } from './home.component';
import { Shell } from '@app/shell/shell.service';
import { BuyProductComponent } from './buy-product/buy-product.component';

const routes: Routes = [
  Shell.childRoutes([
    { path: '', redirectTo: '/products', pathMatch: 'full' },
    { path: 'products', component: HomeComponent, data: { title: marker('Products') } },
    { path: 'products/:id', component: BuyProductComponent, data: { title: marker('product ') } },
  ]),
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
  providers: [],
})
export class HomeRoutingModule {}
