# honeywell REST API Calls with Modules

## Getting started

To get the Node server running locally:

# Clone this repo

**_navigate to cd ./honeywell-api-services/_**

# install all required dependencies

**_npm install_**

# if you get any errors with sharp installation

need to install python and windows build tools to run node-gyp

the best solution works with differents windows (Run in Administrator Powershell) edition is :

**_npm install --global --production windows-build-tools_**

# Project structure

```
src/                        project source code
|- config/                  config
|  |- index.js
|- connections/             connections
|  |- index.js
|- controllers              controllers
|  |- admin/                Admin controllers
|  |  |- index.js
|  |- index.js
|- cors/                    cors
|  |- index.js
|- cron-jobs/               cron jobs
|  |- index.js
|- guards                   route guards
|  |- index.js
|- helpers/
|  |- index.js
|- json-data/
|  |- index.js
|- mails                    mail configuration
|  |- index.js
|- middlewares              middlewares configuration
|  |- index.js
|- models/                  Mongo Collection Schemas
|  |- index.js
|- notifications/           Notifications
|  |- index.js
|- passport                 passport auth for social login
|  |- index.js
|- routes/                  All API routes initiation
|  |- admin/                Admin Routes
|  |  |- index.js
|  |- index.js
|- socket/                  all socket connections initiation
|  |- index.js
|- upload/                  upload media
|  |- index.js
|- utils/
|  |- index.js
|  +- ...                    additional ..
|- .env                     values for environments
|- server.js
|- package.json
```

# Add following in .env file for development

```
 # Environent
NODE_ENV=development

# Server Running Port
PORT=3456
# JWT Credentials
JWT_SECRET=iam-secret-keep-it-as-secret
JWT_ISSUER=iam_the_issuer

# MongoDB Credentials
MONGODB_URL=

# Email Sender Credentials
SENDER_EMAIL_ID=
SENDER_EMAIL_PASSWORD=

# Crypto Credentials
CRYPTO_SECRET_KEY=iam-crypto-secret-key

# Session Credentials
SESSION_SECRET=I_@m-Se<Ret

```

# to run Rest Api Server locally

    ```npm start```

---

## Libraries

- [Express](https://expressjs.com)
- [JWT](https://jwt.io)
- [Socket IO](https://socket.io/)
- [Passport Js](http://www.passportjs.org/)
- [Multer S3](https://github.com/badunk/multer-s3)
- [Winston](https://www.npmjs.com/package/winston)
