'use strict';

const env = require('dotenv');
env.config();
global.ENV = process.env;
if (!ENV.NODE_ENV) {
  console.error('Update/set .env file before you start...');
  process.exit();
}
global.logger = require('./src/helpers/logger');
const express = require('express');
const app = express();
const helmet = require('helmet');
const cookieParser = require('cookie-parser');
const useragent = require('express-useragent');
const path = require('path');
const http = require('http');
const cors = require('cors');
const connections = require('./src/connections');
const session = require('./src/helpers/session');
const errorHandler = require('./src/helpers/error-handler');
const { pagination } = require('./src/middlewares');
class Server {
  constructor() {
    console.log('Starting Server...');
    this.PORT = ENV.PORT || 3456;
    this.routes = require('./src/routes');
    this.server = http.createServer(app);
  }
  enableCors() {
    // ? CORS is a node.js package for providing a Connect/Express middleware that can be used to enable CORS with various options.
    app.use(cors()); // Access-Control-Allow-Origin => Open Cors
    // app.use(cors(require('./src/cors/cors'))); // TODO: to allow/restrict cors for particular domains
  }
  initiateMiddlewares() {
    // ? Helmet helps you secure Express apps by setting various HTTP headers.
    app.use(helmet());

    // ? express-useragent is a simple NodeJS/ExpressJS user-agent middleware exposing user-agent details to your application and views
    app.use(useragent.express());

    // ? to recognize the incoming Request Object as strings or arrays
    app.use(express.urlencoded({ extended: true }));

    // ? to recognize the incoming Request Object as a JSON Object
    app.use(express.json({ limit: '300kb' }));

    app.use(cookieParser());
    app.use(pagination);

    // ?
    if (ENV.NODE_ENV === 'production') {
      app.set('trust proxy', true);
    }
    app.use(session);
    app.use('/media-storage', express.static(path.join(__dirname, 'media-storage')));
    app.use('/', express.static(__dirname + '/dist'));
    app.use((req, res, next) => {
      const begin = Date.now();
      res.setHeader('Access-Control-Allow-Origin', req.header('Origin') ? req.header('Origin') : '*');
      res.setHeader('Access-Control-Allow-Credentials', true);
      res.setHeader('Access-Control-Allow-Methods', 'GET, POST, PUT, PATCH, DELETE, OPTIONS');
      res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With, Content-Type, Authorization');

      res.on('finish', () => {
        const t = Date.now() - begin;
        console.log(req.method, res.statusCode, res.statusMessage, req.originalUrl, t + 'ms');
      });

      if (req.method === 'OPTIONS') res.send(200);
      else next();
    });
  }
  async includeRoutes() {
    app.use(this.routes.init());
  }
  async execute() {
    this.enableCors();
    this.initiateMiddlewares();

    // ? Initiating database/mail Connections
    await connections.init();

    this.includeRoutes();
    app.use(errorHandler);

    this.server.listen(this.PORT, () => {
      logger.info(
        `honeywell API Server is listening on ${
          this.server.address().address !== '::' ? 'https://' + this.server.address().address : 'http://localhost'
        }:${this.server.address().port}/`
      );
      ENV.updated_on = new Date().toISOString();
      if (ENV.NODE_ENV === 'production') {
        console.log = () => {};
        console.error = () => {};
      }
    });
    //angular dist deployement
    app.get('/*', function (req, res, next) {
      res.sendFile(path.resolve('dist/index.html'));
    });
  }
}

const s = new Server();
s.execute();
