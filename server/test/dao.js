//dao.js
const mongoose = require('mongoose');

const userSchema = new mongoose.Schema({
  name: String,
});

const taskSchema = new mongoose.Schema({
  userId: String,
  task: String,
});

module.exports = {
  init: () => mongoose.connect(process.env.DATABASE_URL, { useNewUrlParser: true, useUnifiedTopology: true }),
  close: () => mongoose.disconnect(),
  User: mongoose.model('user', userSchema),
  Task: mongoose.model('task', taskSchema),
};
