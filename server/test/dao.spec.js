//dao.spec.js
const { expect } = require('chai');
const mongoUnit = require('mongo-unit');
const testData = require('./data.json');
const daoUT = require('./dao');

describe('dao', () => {
  before(() => daoUT.init());
  beforeEach(() => mongoUnit.load(testData));
  afterEach(() => mongoUnit.drop());

  it('should find all users', () => {
    return daoUT.User.find().then((users) => {
      expect(users.length).to.equal(2);
      expect(users[0].name).to.equal('test');
    });
  });
  it('should find all tasks for user 1', () => {
    return daoUT.User.find()
      .then((users) => users[0])
      .then((user) => daoUT.Task.find({ userId: user._id }))
      .then((tasks) => {
        expect(tasks.length).to.equal(2);
        expect(tasks[0].task).to.equal('do stuff');
      });
  });
});
