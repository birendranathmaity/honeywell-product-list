'use strict';
const http_status = require('../utils/http-status-codes');

module.exports = (req, res, next) => {
  if (req.user) {
    if (req.user.roles.indexOf('USER') > -1) {
      next();
    } else {
      return res.status(http_status.FORBIDDEN).send({
        type: 'access_denied',
        message: "Role Doesn't have permission for this resource",
      });
    }
  } else {
    return res.status(http_status.UNAUTHORIZED).send({
      type: 'un_authorized',
      message: 'Invalid Token',
    });
  }
};
