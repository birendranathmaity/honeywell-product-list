'use strict';
const mongoose = require('mongoose');

const { modelConstants } = require('../constants');

const schema = mongoose.Schema(
  {
    title: {
      type: String,
      required: true,
      minlength: 2,
    },
    description: {
      type: String,
      default: '',
    },
    category: {
      type: String,
      default: '',
    },
    rating: {
      type: Object,
      default: '',
    },

    price: {
      type: Number,
      required: true,
    },
    image: {
      type: String,
    },
    __v: {
      type: Number,
      select: false,
    },
  },
  {
    versionKey: false,
    timestamps: true,
  }
);

const collectionName = modelConstants.PRODUCT;

const PRODUCT = mongoose.model(collectionName, schema, collectionName);

module.exports = PRODUCT;
