'use strict';
const http_status = require('../utils/http-status-codes');
const common_utils = require('./../utils/common-utils');
module.exports = {
  getUserLocation: async (req, res, next) => {
    try {
      const loc = await common_utils.getUserLocation(req);
      return res.status(http_status.OK).send(loc);
    } catch (error) {
      return next(error);
    }
  },
};
