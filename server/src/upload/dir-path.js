'use strict';

const LOCAL_MEDIA_DIR = './media-storage/';

module.exports.getDirPath = (data) => {
  let path = '';
  switch (data.type) {
    case 'avatar':
      path = 'avatar';
      break;
    default:
      path = '';
      break;
  }
  return LOCAL_MEDIA_DIR + path;
};
