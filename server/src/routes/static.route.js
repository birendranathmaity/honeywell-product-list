'use strict';
const router = require('express').Router();
const { StaticController } = require('../controllers');
router.get('/get-ip-location', StaticController.getUserLocation);
module.exports = router;
