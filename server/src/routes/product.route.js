'use strict';
const router = require('express').Router();
const { ProductsController } = require('../controllers');
router.get('/get-products', ProductsController.getProducts);
router.get('/get-product/:id', ProductsController.getProduct);
module.exports = router;
