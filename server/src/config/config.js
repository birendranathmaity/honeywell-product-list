'use strict';
const config = {
  development: {
    WEB_URL: 'http://localhost:4200/',
    API_URL: `http://localhost:${ENV.PORT}/`,
    API_HOST: `localhost:${ENV.PORT}`,
  },
  production: {
    WEB_URL: '',
    API_URL: '',
    API_HOST: '',
  },
};
module.exports = config[ENV.NODE_ENV];
