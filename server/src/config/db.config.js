'use strict';

module.exports = {
  MONGODB_URL: ENV.MONGODB_URL || 'mongodb://localhost:27017/honeywell',
};
