'use strict';

module.exports.config = require('./config');
module.exports.dbConfig = require('./db.config');
module.exports.jwtConfig = require('./jwt.config');
