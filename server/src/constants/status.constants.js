'use strict';

module.exports = {
  APPROVED: 'APPROVED',
  REJECTEED: 'REJECTED',
  SUSPENDED: 'SUSPENDED',
  PENDING: 'PENDING',
};
